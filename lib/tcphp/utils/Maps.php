<?php

namespace tcphp\utils;

class Maps {

  public static function mergeAdd($one, $other, $exclude = null) {
    foreach ($other as $key => $value) {
      if (!isset($one[$key]) && (is_null($exclude) || !in_array($key, $exclude))) {
        $one[$key] = $other[$key];
      }
    }
    return $one;
  }   
  
  public static function mergeUpdate($one, $other, $exclude = null) {
    foreach ($other as $key => $value) {
      if (isset($one[$key]) && (is_null($exclude) || !in_array($key, $exclude))) {
        $one[$key] = $other[$key];
      }
    }
    return $one;
  }   

  public static function mergeOverwrite($one, $other, $exclude = null) {
    foreach ($other as $key => $value) {
      if (is_null($exclude) || !in_array($key, $exclude)) {
        $one[$key] = $other[$key];
      }
    }
    return $one;
  }
  
  public static function removeKeys($map, $keys) {
      $result = array();
      foreach ($map as $key => $val) {
          if (!in_array($key, $keys)) {
              $result[$key] = $val;
          }
      }
      return $result;
  }
  
  public static function emptyCopy($map) {
      $result = array();
      foreach ($map as $key => $val) {
          $result[$key] = null;
      }
      return $result;
  }

  public static function indexBy($arrayOfMaps, $key) {
      $result = array();
      foreach ($arrayOfMaps as $map) {
          $result[$map[$key]] = $map;
      }
      return $result;
  }
  
}

?>
