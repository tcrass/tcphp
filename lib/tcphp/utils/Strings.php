<?php
namespace tcphp\utils;

class Strings {

    public static function sluggify($str, $sep = '-') {
        $str = Strings::html2str($str);
        $str = Strings::expandUmlauts($str);
        return preg_replace('/\W+/', $sep, strtolower($str));
    }

    public static function html2str($str) {
        $str = preg_replace('/<.*?>/', '', $str);
        return html_entity_decode($str, null, 'UTF-8');
    }
    
    public static function expandUmlauts($str) {
        $str = preg_replace('/ä/', 'ae', $str);
        $str = preg_replace('/ö/', 'oe', $str);
        $str = preg_replace('/ü/', 'ue', $str);
        $str = preg_replace('/Ä/', 'Ae', $str);
        $str = preg_replace('/Ö/', 'Oe', $str);
        $str = preg_replace('/Ü/', 'Ue', $str);
        return $str = preg_replace('/ß/', 'ss', $str);
    }
    
}
?>