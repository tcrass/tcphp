<?php

namespace tcphp\utils;

class Objects {

  public static function instantiate($className, $props = null, $empty = false) {
    $obj = new $className();
    if (! is_null($props)) {
      foreach ($props as $name => $value) {
        $setter = 'set' . ucfirst($name);
        if (method_exists($obj, $setter)) {
          $obj->$setter($empty ? null : $value);
        }
        else {
          try {
            $obj->$name = $empty ? null : $value;
          }
          catch (Exception $ex) {
            $obj[$name] = $empty ? null : $value;
          }
        }
      }
    }
    return $obj;
  }
  
}

?>
