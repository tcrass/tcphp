<?php
namespace tcphp\upload;


abstract class AbstractUploadHandler {

  const TMP_NAME = "tmp_name";
  const UPLOAD_TYPE_FLASH = "uploadType_flash";
  const UPLOAD_TYPE_SINGLE_HTML = "uploadType_singleHtml";
  const UPLOAD_TYPE_MULTIPLE_HTML = "uploadType_multipleHtml";
  const UPLOAD_TYPE_MULTIPLE_HTML5 = "uploadType_multipleHtml5";
  
  protected abstract function processFiles(&$files);
  
  private $fieldName;
  private $flashName;

  public function __construct($fieldName = "uploadedfile", $flashName = null) {
    $this->fieldName = $fieldName;
    $this->flashName = is_null($flashName) ? "${fieldName}Flash" : $flashName;
  }
  
  public function processUploads() {

    $fieldName = $this->fieldName;
    $flashName = $this->flashName;
    
    $uploadType = $_POST['uploadType'];
    $files = array();
    $result = null;

    if ($uploadType == 'flash') {
      //
      // Flash upload
      //

      // TODO
      
    }
    else {

      if ($uploadType == 'html') {
        //
        // Multiple legacy HTML uploads
        //
    
        $i = 0;
        while (isset($_FILES["$fieldName$i"])) {
          array_push($files, $_FILES["$fieldName$i"]);
          $i++;
        }
      
      }
      elseif ($uploadType == "html5") {
        //
        // HTML5 upload
        //
    
        $names = $_FILES["${fieldName}s"]['name'];
        for ($i = 0; $i < count($names); $i++) {
          $file = array();
          foreach ($_FILES["${fieldName}s"] as $key => $val) {
            $file[$key] = $val[$i];
          }
          array_push($files, $file);
        }
      
      }
      elseif (isset($_FILES[$fieldName])) {
        //
        // Single legacy HTML upload
        //
    
        $uploadType = UPLOAD_TYPE_SINGLE_HTML;
        $files = $_FILES[$fieldName];
     }

      $this->processFiles($files);
      $result = json_encode($files);
      
    }
    
    if ($uploadType == "html5") {
      return $result;
    }
    else {
      return "<textarea>$result</textarea>";
    }

  }
  
}
