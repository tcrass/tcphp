<?php 

namespace tcphp\rest;

class RestResult  {

  private $data = null;
  private $code = null;
  private $headers = null;
  
  public function __construct($data, $code = 200, $headers = null) {
    if (is_null($headers)) {
      $headers = array();
    }
    $this->data = $data;
    $this->code = $code;
    $this->headers = $headers;
  }

  public function setData($data) {
    $this->data = $data;
  }
  
  public function getData() {
    return $this->data;
  }
  
  public function setCode($code) {
    $this->code = $code;
  }
  
  public function getCode() {
    return $this->code;
  }
  
  public function getHeaders() {
    return $this->headers;
  }

  public function addHeader($header) {
    array_push($this->headers, $header);
  }
  
}


?>