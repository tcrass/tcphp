<?php

namespace tcphp\rest;

use ReflectionMethod;

require_once 'tcphp/rest/RestResult.php';
require_once 'tcphp/utils/Arrays.php';
require_once 'tcphp/utils/Maps.php';
require_once 'tcphp/utils/net.php';

const COUNT = 'count';
const LIMIT = 'limit';
const RANGE = 'range';
const SORT = 'sort';
const FIELDS = 'fields';
const DAO = 'dao';
const DAO_METHOD = 'daoMethod';
const METHODS = 'methods';
const SEGM_TYPE = 'segmType';
const SEGM_NAME = 'segmName';
const SEGM_STRING = 'segmString';
const SEGM_ARG = 'segmArg';
const SEGM_OPT_ID = 'segmOptId';
const MAPPING_PATTERN = 'mappingPattern';
const MAPPING_PARAMS = 'mappingParams';
const MAPPING_REGEX = 'mappingRegex';
const MAPPING_SEGM_DESCRS = 'mappingSegmDescrs';
const PATH = 'path';
const ID = 'id';
const ARGS = 'args';
const METHOD_NAME = 'methodName';
const SIGNATURE = 'signature';
const BASE_PATH = 'basePath';
const COUNT_PARAM = 'countParam';
const DATA_PARAM = 'dataParam';
const FIELDS_PARAM = 'fieldsParam';
const LIMIT_PARAM = 'limitParam';
const METHOD_PARAM = 'methodParam';
const RANGE_PARAM = 'rangeParam';
const SORT_PARAM = 'sortParam';

abstract class AbstractRestHandler {
  private $options = array (
      BASE_PATH => '',
      COUNT_PARAM => '_count',
      DATA_PARAM => '_data',
      FIELDS_PARAM => '_fields',
      LIMIT_PARAM => '_limit',
      METHOD_PARAM => '_method',
      RANGE_PARAM => '_range',
      SORT_PARAM => '_sort' 
  );
  private $db;
  private $daos;
  private $mappings;
  private $mappingDescriptions = array ();
  private $methodsByPattern = array ();
  
  function __construct($db, $mappings, $options) {
    $this->options = \tcphp\utils\Maps::mergeUpdate($this->options, $options);
    
    $this->db = $db;
    
    $this->mappings = $mappings;
    
    foreach ($mappings as $pattern => $params) {
      $patterh = preg_replace('/\/$/', '', $pattern);
      
      $methods = array (
          'GET' 
      );
      $matches = array ();
      if (preg_match('/^([\w,]+):(.+)$/', $pattern, $matches)) {
        $methods = preg_split('/,/', $matches[1]);
        $pattern = $matches[2];
      }
      
      if (is_null($params)) {
        $params = array ();
      }
      $params[METHODS] = $methods;
      
      
      $mappingDescr = $this->buildMappingDescr($pattern, $params);
      
      if (!isset($this->methodsByPattern[$pattern])) {
        $this->methodsByPattern[$pattern] = array();
      }
      foreach ($methods as $method) {
        if (!isset($this->mappingDescriptions[$method])) {
          $this->mappingDescriptions[$method] = array();
        }
        $this->mappingDescriptions[$method][$pattern] = $mappingDescr;
        if (!in_array($method, $this->methodsByPattern[$pattern])) {
          array_push($this->methodsByPattern[$pattern], $method);
        }
      }
      
    }
    
    $this->daos = array ();
  }

  protected function parseFieldsParamValue($str) {
    return preg_split('/\s*,\s*/', $str);
  }

  protected function parseLimitParamValue($str) {
    // TODO
    return array ();
  }

  protected function parseRangeParamValue($str) {
    // TODO
    return array ();
  }

  protected function parseSortParamValue($str) {
    // TODO
    return array ();
  }

  protected function getData() {
    $data = null;
    if ($_SERVER['REQUEST_METHOD'] != 'GET') {
      if (isset($this->options[DATA_PARAM]) && isset($_POST[$this->options[DATA_PARAM]])) {
        $data = $_POST[$this->options[DATA_PARAM]];
      }
      else {
        $data = file_get_contents('php://input');
      }
    }
    return $data;
  }

  public function handleAsJson() {
    $rawData = $this->getData();
    $data = json_decode($rawData, true);
    
    $result = $this->handleRequest($data);
    $result->setData(json_encode($result->getData()));
    $result->addHeader("Content-Type: application/json");
    
    return $result;
  }

  public function handleRequest(&$data = null) {
    
    global $_DATA;
    
    if (is_null($data)) {
      $data = $this->getData();
    }
    
    $url = parse_url($_SERVER['REQUEST_URI']);
    $path = preg_replace('/\/$/', '', $url['path']);
    if (startsWith($path, $this->options[BASE_PATH])) {
      $path = substr($path, strlen($this->options[BASE_PATH]));
    }
    
    $method = $_SERVER['REQUEST_METHOD'];
    
    $count = false;
    $fields = null;
    $sort = null;
    $filters = array ();
    foreach ($_DATA as $field => $value) {
      $value = urldecode($value);
      if ($field == $this->options[COUNT_PARAM]) {
        $count = empty($value) ? true : filter_var($value, FILTER_VALIDATE_BOOLEAN);
      }
      elseif ($field == $this->options[FIELDS_PARAM]) {
        $fields = $this->parseFieldsParamValue($value);
      }
      elseif ($field == $this->options[LIMIT_PARAM]) {
        $limit = $this->parseLimitParamValue($value);
      }
      elseif ($field == $this->options[METHOD_PARAM]) {
        $method = strtoupper($value);
      }
      elseif ($field == $this->options[RANGE_PARAM]) {
        $range = $this->parseRangeParamValue($value);
      }
      elseif ($field == $this->options[SORT_PARAM]) {
        $sort = $this->parseSortParamValue($value);
      }
      else {
        $filters[$field] = $value;
      }
    }
    $options = array (
        COUNT => $count,
        FIELDS => $fields,
        SORT => $sort 
    );
    
    return $this->handle($method, $path, $filters, $options, $data);
  }

  public function handle($method, $path, $filters = null, $options = null, &$data = null) {
    $handled = false;
    $result = null;
    foreach ($this->mappingDescriptions[$method] as $mappingDescr) {
      $matchResult = $this->match($method, $path, $mappingDescr);
      if (! is_null($matchResult)) {
        $result = $this->exec($mappingDescr, $matchResult, $method, $filters, $options, $data);
        $handled = true;
        break;
      }
    }
    if (! $handled) {
      throw new \Exception("Unable to handle request '$path': no matching pattern found!", 404);
    }
    return $result;
  }

  private function buildMappingDescr($pattern, $params) {
    $segments = preg_split('/\//', $pattern);
    $segmentDescrs = array ();
    $regexs = array ();
    foreach ($segments as $segment) {
      $matches = array ();
      $segmentDescr = array ();
      if (preg_match('/^(\w+)$/', $segment, $matches)) {
        $segmentDescr[SEGM_TYPE] = SEGM_STRING;
        $segmentDescr[SEGM_NAME] = $matches[1];
        if (! isset($params[DAO])) {
          $params[DAO] = $matches[1];
        }
        array_push($regexs, '\/' . $matches[1]);
      }
      elseif (preg_match('/^(\$\d*)$/', $segment, $matches)) {
        $segmentDescr[SEGM_TYPE] = SEGM_ARG;
        $segmentDescr[SEGM_NAME] = $matches[1];
        array_push($regexs, '\/(\w+)');
      }
      else {
        throw new \Exception("Mal-formed segment definition: $segment", 400);
      }
      array_push($segmentDescrs, $segmentDescr);
    }
    
    if (! isset($params[DAO])) {
      throw new \Exception("No dao specified in definition: $pattern!", 400);
    }
    
    if (! isset($params[DAO_METHOD])) {
      array_push($regexs, '(\/(\d+)?)?');
      array_push($segmentDescrs, array (
          SEGM_TYPE => SEGM_OPT_ID 
      ));
    }
    
    $regex = '/^' . join('', $regexs) . '$/';
    
    return array (
        MAPPING_PATTERN => $pattern,
        MAPPING_PARAMS => $params,
        MAPPING_REGEX => $regex,
        MAPPING_SEGM_DESCRS => $segmentDescrs 
    );
  }

  private function match($method, $path, $mappingDescr) {
    $params = $mappingDescr[MAPPING_PARAMS];
    $matches = array ();
    if (preg_match($mappingDescr[MAPPING_REGEX], $path, $matches)) {
      $mi = 1;
      $matchResult = array (
          PATH => $path,
          ID => null,
          ARGS => array () 
      );
      foreach ($mappingDescr[MAPPING_SEGM_DESCRS] as $segmDescr) {
        switch ($segmDescr[SEGM_TYPE]) {
          
          case SEGM_STRING :
            // Do nothing
            break;
          
          case SEGM_ARG :
            $val = $matches[$mi ++];
            $matchResult[ARGS][$segmDescr[SEGM_NAME]] = $val;
            break;
          
          case SEGM_OPT_ID :
            $mi ++;
            if (count($matches) > $mi) {
              $matchResult[ID] = $matches[$mi ++];
            }
            break;
        }
      }
      return $matchResult;
    }
    else {
      return null;
    }
  }

  private function exec($mappingDescr, $matchResult, $method, $filters, $options, &$data) {
    $pattern = $mappingDescr[MAPPING_PATTERN];
    $params = $mappingDescr[MAPPING_PARAMS];
    $dao = $this->getDao($params[DAO]);
    
    $id = $matchResult[ID];
    $args = $matchResult[ARGS];
    
    $count = $options[COUNT];
    $fields = $options[FIELDS];
    
    $daoMethod = null;
    if (isset($params[DAO_METHOD])) {
      $daoMethod = $params[DAO_METHOD];
    }
    else {
      $daoMethod = $this->getDefaultDaoMethod($method, $id, $count, $data);
    }
    $daoMethodDescr = $this->getDaoMethodDescr($daoMethod);
    $daoMethodArgs = $this->buildArgs($daoMethodDescr[SIGNATURE], $data, $id, $filters, $fields, $options, $args);
    
    if (method_exists($dao, $daoMethodDescr[METHOD_NAME])) {
      $methodObj = new ReflectionMethod(get_class($dao), $daoMethodDescr[METHOD_NAME]);
      $resultData = $methodObj->invokeArgs($dao, $daoMethodArgs);
      return $this->createRestResult($mappingDescr, $matchResult, $method, $id, $dao, $options, $resultData);
    }
    else {
      throw new \Exception("Method $daoMethod not implemented in DAO " . get_class($dao) . '!', 501);
    }
  }

  private function getDao($name) {
    if (! array_key_exists($name, $this->daos)) {
      $method = 'create' . ucfirst($name) . 'Dao';
      $this->daos[$name] = $this->$method($this->db);
    }
    return $this->daos[$name];
  }

  private function getDefaultDaoMethod($method, $id, $count, &$data) {
    $daoMethod = null;
    switch ($method) {
      case 'GET' :
        if ($count) {
          $daoMethod = 'countBy(@,%)';
        }
        elseif (! is_null($id)) {
          $daoMethod = 'findById($,[],%)';
        }
        else {
          $daoMethod = 'findAllBy(@,[],%)';
        }
        break;
      case 'PUT' :
        if (\tcphp\utils\Arrays::isSequential($data)) {
          $daoMethod = 'saveOrUpdateAll(!,-,[],%)';
        }
        else {
          $daoMethod = 'saveOrUpdate(!,-,[],%)';
        }
        break;
      case 'POST' :
        if (\tcphp\utils\Arrays::isSequential($data)) {
          $daoMethod = 'saveOrUpdateAll(!,-,[],%)';
        }
        else {
          $daoMethod = 'saveOrUpdate(!,-,[],%)';
        }
        break;
      case 'DELETE' :
        if (! is_null($id)) {
          $daoMethod = 'deleteById($,%)';
        }
        elseif (! is_null($data)) {
          if (\tcphp\utils\Arrays::isSequential($data)) {
            $daoMethod = 'deleteAll(!,-,[],%)';
          }
          else {
            $daoMethod = 'delete(!,-,[],%)';
          }
        }
        else {
          $daoMethod = 'deleteBy(@,%)';
        }
        break;
      default :
        throw new \Exception("No suitable DAO method found.", 501);
    }
    return $daoMethod;
  }

  private function getDaoMethodDescr($daoMethod) {
    $result = null;
    $matches = array ();
    if (preg_match('/^(\w+)\((.*)\)$/', $daoMethod, $matches)) {
      $result = array (
          METHOD_NAME => $matches[1],
          SIGNATURE => $matches[2] 
      );
    }
    return $result;
  }

  private function buildArgs($signature, &$data, $id, $filters, $fields, $options, $args) {
    $result = array ();
    $parts = preg_split('/,/', $signature);
    foreach ($parts as $part) {
      $part = trim($part);
      switch ($part) {
        case '!' :
          array_push($result, $data);
          break;
        case '$' :
          array_push($result, $id);
          break;
        case '@' :
          array_push($result, $filters);
          break;
        case '[]' :
          array_push($result, $fields);
          break;
        case '%' :
          array_push($result, $options);
          break;
        case '-' :
          array_push($result, null);
          break;
        default :
          if (isset($args[$part])) {
            array_push($result, $args[$part]);
          }
          else {
            throw new \Exception("Illegal method signature placeholder: '$part'", 500);
          }
      }
    }
    return $result;
  }

  private function createRestResult($mappingDescr, $matchResult, $method, $id, $dao, $options, $resultData) {
    $code = 200;
    $headers = array ();
    $fullPath = $this->options[BASE_PATH] . $matchResult[PATH];
    switch ($method) {
      case 'GET' :
        if (is_null($resultData)) {
          $code = 204;
        }
        break;
      case 'PUT' :
        if (is_null($id)) {
          if (in_array('GET', $this->methodsByPattern[$mappingDescr[MAPPING_PATTERN]])) {
            if (\tcphp\utils\Arrays::isSequential($resultData)) {
              array_push($headers, "Location: $fullPath");
            }
            else {
              $newId = \tcphp\utils\Arrays::isSequential($resultData) ? null : $dao->getIdentity($resultData, $options);
              array_push($headers, "Location: $fullPath/$newId");
            }
          }          
        }
        break;
      case 'POST' :
        if (is_null($id)) {
          if (in_array('GET', $this->methodsByPattern[$mappingDescr[MAPPING_PATTERN]])) {
            if (\tcphp\utils\Arrays::isSequential($resultData)) {
              array_push($headers, "Location: $fullPath");
            }
            else {
              $newId = \tcphp\utils\Arrays::isSequential($resultData) ? null : $dao->getIdentity($resultData, $options);
              array_push($headers, "Location: $fullPath/$newId");
            }
          }
        }
      break;
      case 'DELETE' :
        $code = 204;
        break;
    }
    return new \tcphp\rest\RestResult($resultData, $code, $headers);
  }
}

?>