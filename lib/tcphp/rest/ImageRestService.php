<?php

namespace tcphp\rest;

require_once('tcphp/rest/AbstractRestService.php');

class ImageRestService extends \tcphp\rest\AbstractRestService {

  public function __construct($handler) {
    parent::__construct($handler);
  }
  
  public function doProcessRequest() {
    $result = $this->getHandler()->handleRequest();
    if ($result->getData() != null) {
      $result->addHeader('Content-Type: ' . $this->getContentType($result->getData()));
      $result->setData($this->getImageData($result->getData()));
    }
    else {
      $result->setData('<html><head><title>Not found.</title></head><body><h1>404: Image not found.</h1></body>');
      $result->setCode(404);
    }
    return $result;
  }
  
  protected function getContentType($result) {
    return 'image/' . $result['type'];
  }
  
  protected function getImageData($result) {
    return $result['data'];
  }
  
}

?>