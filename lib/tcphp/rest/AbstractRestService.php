<?php

namespace tcphp\rest;

abstract class AbstractRestService {
  private $handler;

  public function __construct($handler) {
    $this->handler = $handler;
  }

  protected function getHandler() {
    return $this->handler;
  }

  public function processRequest() {
    try {
      $result = $this->doProcessRequest();
      header(' ', true, $result->getCode());
      foreach ($result->getHeaders() as $header) {
        header($header);
      }
      return $result->getData();
    }
    catch (\Exception $ex) {
      $message = $ex->getMessage(); 
      $code = $ex->getCode() ? $ex->getCode(): 500; 
      header("X-Error-Message: $message", true, $code);
      header("Content-type: text/html");
      return "<html><head><title>A code $code error occured.</title></head><body><h1>$code: An error occured.</h1><p>$message</p></body>";
    }
  }

  protected abstract function doProcessRequest();
}

?>