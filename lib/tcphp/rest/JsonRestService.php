<?php

namespace tcphp\rest;

//header('Content-Type: application/json; charset=utf-8');

require_once('tcphp/rest/AbstractRestService.php');

class JsonRestService extends \tcphp\rest\AbstractRestService {

  public function __construct($handler) {
    parent::__construct($handler);
  }

  public function doProcessRequest() {
    return $this->getHandler()->handleAsJson();
  }  

}

?>