<?php

namespace tcphp\storage;

require_once 'tcphp/utils/Arrays.php';
require_once 'tcphp/utils/Objects.php';

const ASC = 'asc';
const AUTO_INC_ID = 'storage_autoIncId';
const CLASS_NAME = 'storage_className';
const COLUMN_NAMES = 'storage_columnNames';
const DESC = 'desc';
const DESCENDING = 'descending';
const DIRECTION = 'direction';
const FIELD = 'field';
const FIELD_TYPES = 'storage_fieldTypes';
const FILTER_STRINGS = 'storage_filterStrings';
const FLATTEN_TO = 'storage_flattenTo';
const ID_FIELD = 'storage_idField';
const INDEX_BY = 'storage_indexBy';
const KEY_FIELDS = 'storage_keyFields';
const NEGATE = 'storage_negate';
const OPERATOR = 'storage_operator';
const SORT = 'sort';
const TABLE = 'storage_table';

abstract class AbstractReadOnlyDao {
  protected $db;
  private $fieldTypes;
  private $fields;
  private $columnNames;

  protected function TABLE() {
    return null;
  }

  protected abstract function FIELD_TYPES();

  protected function COLUMN_NAMES() {
    return array ();
  }

  public function ID_FIELD() {
    return null;
  }

  protected function AUTO_INC_ID() {
    return false;
  }

  protected function KEY_FIELDS() {
    return null;
  }

  protected function FIND_ALL_SQL($options = null) {
    return 'SELECT * FROM ' . $this->getTable($options);
  }

  protected function FIND_BY_SQL($options = null) {
    return $this->FIND_ALL_SQL($options) . ' WHERE';
  }

  public function __construct($db) {
    $this->db = $db;
  }

  public function getIdentity(&$item, $options) {
    $keyFields = $this->getKeyFields($options);
    if (count($keyFields) == 1) {
      return $item[$keyFields[0]];
    }
    else {
      throw new \Exception("Only simple keys are supported!");
    }
  }
  
  public function findBy($conditions, $returnFields = null, $options = null) {
    $params = $this->buildSelectParams($conditions, $returnFields, $options);
    
#    echo('<p>' . $params[SQL] . ' --  ' . join(',', $params[VALUES]) . '');
    
    $result = $this->db->selectOne($params [SQL], $params [RESULT_TYPES], $params [VALUES]);
    
#    echo('<p>Success!</p>');
    
    if (is_null($result)) {
      return null;
    }
    else {
      if (!empty($options[CLASS_NAME])) {
        return \tcphp\utils\Objects::instantiate($options[CLASS_NAME], $result);
      }
      else if (!empty($options[FLATTEN_TO])) {
        return $result[$options[FLATTEN_TO]];
      }
      else {
        return $result;
      }
    }
  }

  public function findById($id, $returnFields = null, $options = null) {
    return $this->findBy(array (
        $this->getFilterString($this->getIdField(), $id) => $id 
    ), $returnFields, $options);
  }

  public function findAll($returnFields = null, $options = null) {
    return $this->findAllBy(null, $returnFields, $options);
  }

  public function findAllBy($conditions, $returnFields = null, $options = null) {
    if (is_null($options)) {
      $options = array();
    }
    $params = $this->buildSelectParams($conditions, $returnFields, $options);

#    echo('<p>' . $params[SQL] . ' --  ' . join(',', $params[VALUES]) . '');
    
    $result = $this->db->selectAll($params [SQL], $params [RESULT_TYPES], $params [VALUES]);

#    echo('<p>Success!</p>');
    
    if (!empty($options[CLASS_NAME])) {
      $result = $this->toObjects($result, $options);
    }
    
    if (empty($options [INDEX_BY]) && empty($options [FLATTEN_TO])) {
      return $result;
    }
    elseif (!empty($options[INDEX_BY])) {
      $reindexed = array ();
      foreach ($result as $item) {
        $reindexed [$item [$options [INDEX_BY]]] = !empty($options[FLATTEN_TO]) ? $item[$options['flatten']] : $item;
      }
      return $reindexed;
    }
    else {
      $flattened = array();
      foreach ($result as $item) {
        array_push($flattened, $item[$options[FLATTEN_TO]]);
      }
      return $flattened;
    }
  }
  
  private function toObjects($items, $options) {
    $result = array();
    foreach ($items as $item) {
      array_push($result, \tcphp\utils\Objects::instantiate($options[CLASS_NAME], $item));
    }
    return $result;
  }

  public function exists(&$item, $keyFields = null, $options = null) {
    if ($this->getAutoIncId($options)) {
      if (isset($item [$this->getIdField($options)])) {
        return $this->existsById($item [$this->getIdField($options)]);
      }
      else {
        return false;
      }
    }
    else {
      $conditions = $this->buildKeyConditions($item, $keyFields, $options);
      if (count($conditions) == 0) {
        return false;
      }
      else {
        return $this->existsBy($conditions, $options);
      }
    }
  }

  public function existsBy($conditions, $options = null) {
    return ! is_null($this->findBy($conditions, null, $options));
  }

  public function existsById($id, $options = null) {
    return ! is_null($this->findById($id, $options));
  }

  public function count($options = null) {
    return count($this->findAll($options));
  }

  public function countBy($conditions, $options = null) {
    return count($this->findBy($conditions, null, $options));
  }

  protected function getTable($options = null) {
    if (isset($options [TABLE])) {
      return $options [TABLE];
    }
    else {
      return $this->TABLE();
    }
  }

  protected function getFields($options = null) {
    $fields = null;
    if (isset($options [FIELD_TYPES])) {
      $fields = array_keys($options [FIELD_TYPES]);
    }
    else {
      if (! isset($this->fields)) {
        $fieldTypes = $this->getFieldTypes();
        $this->fields = array_keys($fieldTypes);
      }
      $fields = $this->fields;
    }
    return $fields;
  }

  protected function getFieldTypes($options = null) {
    $fieldTypes = null;
    if (isset($options [FIELD_TYPES])) {
      $fieldTypes = $options [FIELD_TYPES];
    }
    else {
      if (! isset($this->fieldTypes)) {
        $this->fieldTypes = $this->FIELD_TYPES();
      }
      $fieldTypes = $this->fieldTypes;
    }
    return $fieldTypes;
  }

  protected function getFieldType($field, $options = null) {
    $fieldTypes = $this->getFieldTypes($options);
    return isset($fieldTypes[$field]) ? $fieldTypes[$field] : null;
  }

  protected function getColumnNames($options = null) {
    $columnNames = null;
    if (isset($options [COLUMN_NAMES])) {
      $columnNames = $options [COLUMN_NAMES];
    }
    else {
      if (! isset($this->columnNames)) {
        $this->columnNames = $this->COLUMN_NAMES();
      }
      $columnNames = $this->columnNames;
    }
    return $columnNames;
  }

  protected function getColumnName($field, $options = null) {
    $columnNames = $this->getColumnNames($options);
    if (isset($columnNames [$field])) {
      return $columnNames [$field];
    }
    else {
      return $field;
    }
  }

  protected function getQuotedColumnName($field, $options = null) {
    return $this->quoteName($this->getColumnName($field, $options));
  }

  protected function getAliasedColumnName($field, $options) {
    $colName = $this->getColumnName($field, $options);
    $result = $this->quoteName($colName);
    if ($colName != $field) {
      $result .= ' AS ' . $this->quoteName($field);
    }
    return $result;
  }

  protected function quoteName($colName) {
    $parts = preg_split('/\./', $colName);
    $quoted = array ();
    foreach ($parts as $part) {
      array_push($quoted, $this->db->getConnection()->quoteIdentifier($part));
    }
    return join('.', $quoted);
  }

  protected function getIdField($options = null) {
    $idField = null;
    if (isset($options [ID_FIELD])) {
      $idField = $options [ID_FIELD];
    }
    else {
      $idField = $this->ID_FIELD();
      if (is_null($idField)) {
        throw new \Exception("This DAO doesn't support operations based on ID!");
      }
    }
    return $idField;
  }

  protected function getAutoIncId($options = null) {
    if (isset($options [AUTO_INC_ID])) {
      return $options [AUTO_INC_ID];
    }
    else {
      return $this->AUTO_INC_ID();
    }
  }

  protected function getKeyFields($options) {
    $keyFields = null;
    if (isset($options [KEY_FIELDS])) {
      $keyFields = $options [KEY_FIELDS];
    }
    else {
      $keyFields = $this->KEY_FIELDS();
    }
    if (is_null($keyFields)) {
      try {
        $keyFields = array (
            $this->getIdField($options) 
        );
      }
      catch (\Exception $ex) {
      }
    }
    if (is_null($keyFields)) {
      $keyFields = $this->getFields($options);
    }
    return $keyFields;
  }

  protected function getFilterString($field, $value, $options = null) {
    $column = $this->getQuotedColumnName($field, $options);
    if (is_null($value)) {
      if ($options [NEGATE]) {
        return "$column IS NOT NULL";
      }
      else {
        return "$column IS NULL";
      }
    }
    else {
      $operator = isset($options [OPERATOR]) ? $options [OPERATOR] : '=';
      return "$column $operator ?";
    }
  }

  protected function buildSelectParams($conditions, $returnFields, $options = null) {
    if (is_null($options)) {
      $options = array();
    }
    if (is_null($conditions)) {
      $conditions = array ();
    }
    if (is_null($returnFields)) {
      $returnFields = array_keys($this->getFieldTypes($options));
    }
    else {
      if (!is_array($returnFields)) {
        $returnFields = array($returnFields);
      }
    }
    
    $columnNames = array ();
    $resultTypes = array ();
    foreach ($returnFields as $field) {
      array_push($columnNames, $this->getAliasedColumnName($field, $options));
      array_push($resultTypes, $this->getFieldType($field, $options));
    }
    $selectColumns = join(', ', $columnNames);
    
    $placeholderValues = array ();
    $filterStrings = $this->buildFilterStrings($conditions, $placeholderValues, $options);
    
    $sql;
    if (count($filterStrings) == 0) {
      $sql = $this->FIND_ALL_SQL($options);
    }
    else {
      $sql = $this->FIND_BY_SQL($options) . ' ' . join(' AND ', $filterStrings);
    }
    $sql = preg_replace('/\s+\*\s+/', ' ' . $selectColumns . ' ', $sql);
    $sql .= ' ' . $this->buildOrderBy($options);
    
    return array (
        SQL => $sql,
        RESULT_TYPES => $resultTypes,
        VALUES => $placeholderValues 
    );
  }

  protected function buildKeyConditions(&$item, $keyFields = null, $options) {
    if (is_null($keyFields)) {
      $keyFields = $this->getKeyFields($options);
    }
    $conditions = array ();
    foreach ($keyFields as $field) {
      if (isset($item [$field])) {
        $conditions [$this->getFilterString($field, $item [$field])] = $item [$field];
      }
    }
    return $conditions;
  }

  protected function buildFilterStrings($conditions, &$valueCollector, $options) {
    if (is_null($conditions)) {
      $conditions = array ();
    }
    $filterStrings = array ();
    foreach ($conditions as $filterString => $values) {
      if (! is_array($values)) {
        $values = array (
          $values
        );
      }
      if (!is_null($this->getFieldType($filterString, $options))) {
        if (is_null($values[0])) {
          array_push($filterStrings, $this->getQuotedColumnName($filterString, $options) . " IS NULL");
        }
        else {
          array_push($filterStrings, $this->getQuotedColumnName($filterString, $options) . " = ?");
        }
      }
      else {
        array_push($filterStrings, $filterString);
      }
      foreach ($values as $value) {
        if (!is_null($value)) {
          array_push($valueCollector, $value);
        }
      }
    }
    return $filterStrings;
  }

  protected function buildOrderBy($options) {
    $sortingSpecs = $this->buildSortingSpecs($options);
    $orderBy = array();
    foreach ($sortingSpecs as $spec) {
      array_push($orderBy, 'ORDER BY ' . $this->getQuotedColumnName($spec[FIELD], $options) . ' ' . strtoupper($spec[DIRECTION]));
    }
    return join(' ', $orderBy);
  }
  
  protected function buildSortingSpecs($options = null) {
    $sortingSpecs = array();
    if (!empty($options[SORT])) {
      $sortOpts = $options[SORT];
      if (!\tcphp\utils\Arrays::isSequential($sortOpts)) {
        if (is_array($sortOpts)) {
          $sortOpts = array($sortOpts);
        }
        else {
          $parts = preg_split('/\s*,\s*/', $sortOpts);
          $sortOpts = array();
          foreach ($parts as $part) {
            $matches = array();
            if (preg_match('/^([-+])?(.+)/', $part, $matches)) {
              array_push($sortOpts, array(
                FIELD => $matches[2],
                DESCENDING => $matches[1] == '-'
              ));
            }
          }
        }
      }
      foreach ($sortOpts as $sortOpt) {
        if (!empty($sortOpt[DESCENDING])) {
          $sortOpt[DIRECTION] = DESC;
        }
        elseif (empty($sortOpt[DIRECTION])) {
          $sortOpt[DIRECTION] = ASC;
        }
        array_push($sortingSpecs, $sortOpt);
      }
    }
    return $sortingSpecs;
  }
  
  
}

?>