<?php

namespace tcphp\storage;

const CHARSET = "stoarge_charset";
const RESULT_TYPES = 'storage_resultTypes';
const SQL = 'storage_sql';
const TYPES = 'storage_types';
const VALUES = 'storage_values';

class DB {
    private static $instances = array ();
    private $dbCon;
    private $conStr;
    private $options;

    public static function instance($conStr, $options = null) {
        if (!array_key_exists($conStr, self::$instances)) {
            self::$instances [$conStr] = new DB($conStr, $options);
        }
        return self::$instances [$conStr];
    }

    function __construct($conStr, $options = null) {
        $this->conStr = $conStr;
        $this->options = is_null($options) ? array () : $options;
        
        $config = new \Doctrine\DBAL\Configuration();
        $connectionParams = array (
                        'url' => $conStr 
        );
        
        if (isset($options [CHARSET])) {
            $connectionParams ['charset'] = $options [CHARSET];
        }
        
        $this->dbCon = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
    }

    public function getConnection() {
        return $this->dbCon;
    }

    public function getConnectionString() {
        return $this->conStr;
    }

    public function selectAll($sql, $resultTypes = null, $conditionValues = null) {
        if (!isset($conditionValues)) {
            $conditionValues = array ();
        }
        
        if (preg_match('/FROM house INNER JOIN grid/', $sql)) {
            $foo = 0;
        }
        
        $result = $this->dbCon->fetchAll($sql, $conditionValues);
        if ($resultTypes) {
            foreach ( $result as &$row ) {
                $this->convertRow($row, $resultTypes);
            }
        }
        
        return $result;
    }

    public function selectOne($sql, $resultTypes = null, $conditionValues = null) {
        if (!isset($conditionValues)) {
            $conditionValues = array ();
        }
        
        $result = $this->dbCon->fetchAssoc($sql, $conditionValues);
        if ($result && $resultTypes) {
            $this->convertRow($result, $resultTypes);
        }
        
        return $result ? $result : null;
    }

    public function execute($sql, $placeholderValues) {
        $result = $this->dbCon->executeUpdate($sql, $placeholderValues);
        
        return $result;
    }

    public function lastInsertId() {
        return $this->dbCon->lastInsertID();
    }

    private function createTypes(&$typeNames) {
        $types = array ();
        foreach ( $typeNames as $typeName ) {
            $type = \Doctrine\DBAL\Types\Type::getType($typeName);
            array_push($types, $type);
        }
        return $types;
    }

    private function convertRow(&$row, &$types) {
        $fieldNames = array_keys($row);
        for($i = 0; $i < count($fieldNames); $i++) {
            $name = $fieldNames [$i];
            $row [$name] = $this->dbCon->convertToPHPValue($row [$name], $types [$i]);
            if ($types [$i] == 'clob' || $types [$i] == 'blob') {
                $row [$name] = $this->readLOB($row[$name]);
            }
        }
    }

    private function readLOB($lob) {
        $val = null;
        if (is_resource($lob)) {
            $val = '';
            while ( !feof($lob) ) {
                $val .= fread($lob, 8192);
            }
        }
        return $val;
    }

}

?>