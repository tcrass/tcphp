<?php

namespace tcphp\storage;

require_once ('tcphp/storage/AbstractReadOnlyDao.php');
const SETTABLE_FIELDS = 'storage_settableFiels';

abstract class AbstractReadWriteDao extends \tcphp\storage\AbstractReadOnlyDao {

  protected function SETTABLE_FIELDS() {
    return null;
  }

  protected function UPDATE_ALL_SQL($options = null) {
    return 'UPDATE ' . $this->getTable($options) . ' SET *';
  }

  protected function UPDATE_BY_SQL($options = null) {
    return $this->UPDATE_ALL_SQL($options) . ' WHERE';
  }

  protected function INSERT_SQL($options = null) {
    return 'INSERT INTO ' . $this->getTable($options) . ' * VALUES';
  }

  protected function DELETE_ALL_SQL($options) {
    return 'DELETE FROM ' . $this->getTable($options);
  }

  protected function DELETE_BY_SQL($options = null) {
    return $this->DELETE_ALL_SQL($options) . ' WHERE';
  }

  protected function update(&$item, $setFields = null, $conditions = null, $options = null) {
    $params = $this->buildUpdateParams($item, $setFields, $conditions, $options);
    return $this->db->execute($params [SQL], $params [VALUES]);
  }

  public function saveOrUpdate(&$item, $keyFields = null, $setFields = null, $options = null) {
    if ($this->exists($item, $keyFields, $options)) {
      $this->update($item, $setFields, $this->buildKeyConditions($item, $keyFields, $options), $options);
      return $item;
    }
    else {
      $params = $this->buildInsertParams($item, $setFields, $options);
      $n = $this->db->execute($params [SQL], $params [VALUES]);
      if ($this->getAutoIncId($options)) {
        $item [$this->getIdField($options)] = $this->db->lastInsertID();
      }
      return $item;
    }
  }

  public function saveOrUpdateAll(&$items, $keyFields = null, $setFields = null, $options = null) {
    foreach ($items as $item) {
      $this->saveOrUpdate($item, $keyFields, $setFields, $options);
    }
    return $items;
  }

  public function delete(&$item, $keyFields = null, $options = null) {
    $conditions = $this->buildKeyConditions($item, $keyFields, $options);
    $this->deleteBy($conditions, $options);
  }

  public function deleteBy($conditions, $options = null) {
    $params = $this->buildDeleteParams($conditions, $options);
    return $this->db->execute($params [SQL], $params [VALUES]);
  }

  public function deleteById($id, $options = null) {
    return $this->deleteBy(array (
        $this->getFilterString($this->getIdField($options), $id) => $id 
    ), $options);
  }

  public function deleteAll(&$items, $keyFields = null, $options = null) {
    $n = 0;
    foreach ($items as $item) {
      $n += $this->delete($item, $keyFields, $options);
    }
    return $n;
  }

  protected function buildUpdateParams(&$item, $setFields, $conditions, $options) {
    if (is_null($setFields)) {
      $setFields = $this->getSettableFields($options);
    }
    else {
      if (!is_array($setFields)) {
        $setFields = array($setFields);
      }
    }
    if (is_null($conditions)) {
      $conditions = array ();
    }
    
    $setColumns = array ();
    $placeholderValues = array ();
    foreach ($setFields as $field) {
      if (array_key_exists($field, $item)) {
        array_push($setColumns, $this->getQuotedColumnName($field, $options) . ' = ?');
        array_push($placeholderValues, isset($item [$field]) ? $item [$field] : null);
      }
    }
    $setColumns = join(', ', $setColumns);
    
    $filterStrings = $this->buildFilterStrings($conditions, $placeholderValues, $options);
    
    $sql;
    if (count($filterStrings) == 0) {
      $sql = $this->UPDATE_ALL_SQL($options);
    }
    else {
      $sql = $this->UPDATE_BY_SQL($options) . ' ' . join(' AND ', $filterStrings);
    }
    $sql = preg_replace('/\s+\*\s*/', ' ' . $setColumns . ' ', $sql);
    
    return array (
        SQL => $sql,
        VALUES => $placeholderValues 
    );
  }

  protected function buildInsertParams(&$item, $setFields, $options) {
    if (is_null($setFields)) {
      $setFields = $this->getSettableFields($options);
    }
    
    $columnNames = array ();
    $placeholderStrings = array ();
    $placeholderValues = array ();
    foreach ($setFields as $field) {
      if (array_key_exists($field, $item)) {
        array_push($columnNames, $this->getQuotedColumnName($field, $options));
        array_push($placeholderStrings, '?');
        array_push($placeholderValues, isset($item [$field]) ? $item [$field] : null);
      }
    }
    $insertColumns = join(', ', $columnNames);
    $placeholders = join(', ', $placeholderStrings);
    
    $sql = preg_replace('/\s+\*\s+/', ' (' . $insertColumns . ') ', $this->INSERT_SQL($options)) . ' (' . $placeholders . ')';
    
    return array (
        SQL => $sql,
        VALUES => $placeholderValues 
    );
  }

  protected function buildDeleteParams($conditions, $options) {
    if (is_null($conditions)) {
      $conditions = array ();
    }
    
    $placeholderValues = array ();
    $filterStrings = $this->buildFilterStrings($conditions, $placeholderValues, $options);
    
    $sql;
    if (count($filterStrings) == 0) {
      $sql = $this->DELETE_ALL_SQL($options);
    }
    else {
      $sql = $this->DELETE_BY_SQL($options) . ' ' . join(' AND ', $filterStrings);
    }
    
    return array (
        SQL => $sql,
        VALUES => $placeholderValues 
    );
  }

  protected function getSettableFields($options) {
    $settableFields = null;
    if (isset($options [SETTABLE_FIELDS])) {
      $settableFields = $options [SETTABLE_FIELDS];
    }
    else {
      $settableFields = $this->SETTABLE_FIELDS();
    }
    if (is_null($settableFields)) {
      $settableFields = $this->getFields($options);
      try {
        $idField = $this->getIdField($options);
        $settableFields = array_diff($settableFields, array (
            $idField 
        ));
      }
      catch (\Exception $ex) {
      }
    }
    return $settableFields;
  }
}

?>